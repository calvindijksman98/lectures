site_name: Open Solid State Notes
repo_url: https://gitlab.kwant-project.org/solidstate/lectures
repo_name: source
edit_uri: edit/master/src/
site_description: |
  Lecture notes for the TU Delft course TN2844 Solid state physics
nav:
  - Preface: 'index.md'
  - Phonons and specific Heat:
      - Einstein model: '1_einstein_model.md'
      - Debye model: '2_debye_model.md'
  - Free electron model:
      - Drude model: '3_drude_model.md'
      - Sommerfeld model: '4_sommerfeld_model.md'
  - Atoms and bonds:
      - LCAO model: '5_atoms_and_lcao.md'
      - Bonds and spectra: '6_bonds_and_spectra.md'
  - Electrons and phonons in 1D:
    - Tight-binding model: '7_tight_binding.md'
    - Many atoms per unit cell: '8_many_atoms.md'
  - Crystal structure and diffraction:
    - Crystal structure: '9_crystal_structure.md'
    - X-ray diffraction: '10_xray.md'
  - Tight binding and nearly free electrons:
    - Nearly free electron model: '11_nearly_free_electron_model.md'
    - Band structures in 2D: '12_band_structures_in_higher_dimensions.md'
  - Semiconductors:
    - Basic principles: '13_semiconductors.md'
    - Doping and devices: '14_doping_and_devices.md'
  - Summary and review: '15_SummaryAndReview.md'
  - Attic:
    - Magnetism: 'lecture_8.md'
    - Fermi surface periodic table: 'fermi_surfaces.md'
    - Extra exercises: 'extra_exercises.md'
  - Solutions:
    - Einstein model: '1_einstein_model_solutions.md'
    - Debye model: '2_debye_model_solutions.md'
    - Drude model: '3_drude_model_solutions.md'
    - Sommerfeld model: '4_sommerfeld_model_solutions.md'
    - LCAO model: '5_atoms_and_lcao_solutions.md'
    - Bonds and spectra: '6_bonds_and_spectra_solutions.md'
    - Tight-binding model: '7_tight_binding_model_solutions.md'
    - Many atoms per unit cell: '8_many_atoms_solutions.md'
    - Crystal structure: '9_crystal_structure_solutions.md'
    - X-ray diffraction: '10_xray_solutions.md'
    - Nearly free electron model: '11_nearly_free_electron_model_solutions.md'
    - Band structures in 2D: '12_band_structures_in_higher_dimensions_solutions.md'
    - Basic principles: '13_semiconductors_solutions.md'
    - Doping and devices: '14_doping_and_devices_solutions.md'

theme:
  name: material
  custom_dir: theme
  palette:
    primary: 'white'
    accent: 'indigo'

markdown_extensions:
  - mdx_math:
      enable_dollar_delimiter: True
  - toc:
      permalink: True
  - admonition
  - pymdownx.details
  - pymdownx.extra
  - abbr
  - footnotes
  - meta

extra_css:
  - 'https://use.fontawesome.com/releases/v5.8.1/css/all.css'
  - 'styles/thebelab.css'

extra_javascript:
  - 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/MathJax.js?config=TeX-AMS_HTML'
  - 'https://unpkg.com/thebelab@0.5.0/lib/index.js'
  - 'scripts/mathjaxhelper.js'
  - 'scripts/thebelabhelper.js'
  - 'scripts/x3dom.js'
copyright: "Copyright © 2017-2019 Delft University of Technology, CC-BY-SA 4.0."
