```python tags=["initialize"]
from matplotlib import pyplot as plt 
import numpy as np
from math import pi
```
# Solutions for lecture 9 exercises

## Exercise 1: Diatomic crystal¶

### Subquestion 1
```python
y = np.repeat(np.arange(0,8,2),4)
x = np.tile(np.arange(0,8,2),4)
plt.figure(figsize=(5,5))
plt.axis('off')

# WZ
plt.plot([5,5,7,7,5],[5,7,7,5,5], color='k',ls=':')
plt.annotate('WZ',(6,6.5),fontsize=14,ha='center')

# PUC1 
plt.plot([0,2,4,2,0],[4,6,6,4,4], color='k',ls=':')

# UPC2
plt.plot([6,4,2,4,6],[0,0,2,2,0], color='k',ls=':')

plt.plot(x,y,'ko', markersize=15)
plt.plot(x+1,y+1, 'o', markerfacecolor='none', markeredgecolor='k', markersize=15);
```
### Subquestion 2

In case of different particles, $V = a^2$. 

If identical particles, nearest neighbour distance becomes $a^* = \frac{a}{\sqrt{2}}$ and so $V^* = {a^*}^2 = \frac{a^2}{2}$

### Subquestion 3

$\mathbf{a_1} = a \hat{\mathbf{x}}, \quad \mathbf{a_2} = a \hat{\mathbf{y}}$

Basis: 

$\Huge \bullet \normalsize = (0,0)$

$\bigcirc = (\frac{1}{2},\frac{1}{2})$

### Subquestion 4

Cubic lattice

Basis: 

$\Huge \bullet \normalsize = (0,0,0)$

$\bigcirc = (\frac{1}{2},\frac{1}{2},\frac{1}{2})$

Example: Cesium Chloride (CsCl)

### Subquestion 5
BCC lattice

Example: Sodium (Na)

### Subquestion 6
See notes 

??? hint "What to do?"

    Relate radius of the atom $R$ to the lattice parameter $a$ by considering corner atom touching the center one
    
## Exercise 2: Diamond lattice
### Subquestion 1

It is made up from two FCC lattices

$$
\mathbf{a_1} = \frac{a}{2} \left(\hat{\mathbf{x}}+\hat{\mathbf{y}} \right) \\
\mathbf{a_2} = \frac{a}{2} \left(\hat{\mathbf{x}}+\hat{\mathbf{z}} \right) \\
\mathbf{a_3} = \frac{a}{2} \left(\hat{\mathbf{y}}+\hat{\mathbf{z}} \right)
$$

Basis = $ (0,0,0), (\frac{1}{4},\frac{1}{4},\frac{1}{4})$

### Subquestion 2

2 atoms in a primitive unit cell

??? hint "Why 2 atoms?"

    The basis for the lattice (defined in terms of the primitive lattice vectors) has two atoms. Keep in mind that atoms and lattice points are NOT equivalent in most cases!

$V = \left| \mathbf{a_1} \cdot \left(\mathbf{a_2} \times \mathbf{a_3} \right) \right| = \frac{a^3}{4}$

### Subquestion 3

1 FCC lattice has 4 atoms. Therefore, 2 combined fcc lattices will have 8 atoms

$V = a^3$

### Subquestion 4 

??? hint "Visual hint"

    Consider the atom at (0.25,0.25,0.25) coordinates in the interactive diamond lattice image. Look at its neighbours.

4 nearest neighbours: $-(\frac{1}{4},\frac{1}{4},\frac{1}{4})$ and $-(\frac{1}{4},\frac{1}{4},\frac{1}{4}) + \mathbf{a_i}  $ where $i = 1,2,3$

### Subquestion 5

34%

??? hint "Hint"

    The nearest neighbour atoms should be touching for most efficient packing
    

## Exercise 3: Directions and Spacings of Miller planes
### Subquestion 1

Miller plane - plane that intersects an infinite number of lattice points

Miller index - Set of 3 integers which specify a set of parallel planes

### Subquestion 2

??? hint "Small hint"

    The $(hkl)$ plane intersects lattice at position vectors of $\frac{\mathbf{a_1}}{h}, \frac{\mathbf{a_2}}{k}, \frac{\mathbf{a_1}}{l}$. Can you define a general vector inside the $(hkl)$ plane? 
    
??? hint "Anoter small hint"

    What vector operation takes two vectors and produces another vector that is perpendicular to the previous two? 

??? hint "Last small hint"

    Don't forget to normalize your direction vectors!
    
### Subquestion 3

Same hints as in Subquestion 2

### Subquestion 4

??? hint "Big hint"

    There is always a neighbouring lattice plane which intersects the (0,0,0) lattice point.
    
??? hint "Small hint"

    Don't forget to reuse your unit normal of a plane from Subquestion 2.
