# Open Solid State Notes

!!! summary "Learning goals"

    After following this course you will be able to:

    - Formulate the main models of solid state physics
    - Given a description of a solid state physics problem, write down the corresponding equations
    - Identify the relevant approximations and apply them to solve this problem

In these notes our aim is to provide learning materials which are:

- self-contained
- easy to modify and remix, so we provide the full source, including the code
- open for reuse: see the license below.

Whether you are a student taking this course, or an instructor reusing the materials, we welcome all contributions, so check out the [course repository](https://gitlab.kwant-project.org/solidstate/lectures), especially do [let us know](https://gitlab.kwant-project.org/solidstate/lectures/issues/new?issuable_template=typo) if you see a typo!
